install:
	php artisan migrate:fresh --seed
	php artisan storage:link
	php artisan key:generate
	php artisan scribe:generate
	php artisan optimize:clear
docs:
	php artisan scribe:generate
