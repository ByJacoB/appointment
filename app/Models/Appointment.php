<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Appointment extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'therapist_id',
        'date',
        'time',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function therapist(): BelongsTo
    {
        return $this->belongsTo(Therapist::class);
    }

    public function getAppointmentDateAttribute(): string
    {
        $datetime = $this->date . ' ' . date('H:i', strtotime($this->time));

        if (auth('therapist')->check()) {
            $datetime = $this->convertTimezoneDate($datetime, $this->therapist->timezone);
        } else {
            $datetime = $this->convertTimezoneDate($datetime, $this->user->timezone);
        }

        return $datetime;
    }


    private static function convertTimezoneDate($datetime, $timezone): string
    {
        return Carbon::createFromFormat('Y-m-d H:i', $datetime, 'UTC')
            ->timezone($timezone)->format('Y-m-d H:i');
    }

}
