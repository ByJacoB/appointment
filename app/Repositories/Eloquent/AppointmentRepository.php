<?php

namespace App\Repositories\Eloquent;

use App\Models\Appointment;
use App\Repositories\Eloquent\Interfaces\AppointmentRepositoryInterface;

class AppointmentRepository extends BaseRepository implements AppointmentRepositoryInterface
{
    public function __construct(Appointment $model)
    {
        parent::__construct($model);
    }
}
