<?php

namespace App\Repositories\Eloquent\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface TherapistRepositoryInterface
{
    public function appointments(int $therapistId, int $limit = 24): mixed;

    public function setAvailableHours(int $therapistId, string $time): array;

    public function getAvailableHours(int $therapistId): array;
}
