<?php

namespace App\Repositories\Eloquent\Interfaces;


interface UserRepositoryInterface
{
    public function appointments(int $userId, int $limit = 24): mixed;

    public function createAppointment(int $userId, array $data): mixed;

    public function checkIfAppointmentExists(int $userId, array $data): bool;

    public function getAvailableHours($availableHours, string $userTimezone): array;

}
