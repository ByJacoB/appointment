<?php

namespace App\Repositories\Eloquent;

use App\Models\Therapist;
use App\Repositories\Eloquent\Interfaces\TherapistRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TherapistRepository extends BaseRepository implements TherapistRepositoryInterface
{
    public function __construct(Therapist $model)
    {
        parent::__construct($model);
    }

    public function appointments(int $therapistId, int $limit = 24): mixed
    {
        $therapist = $this->model->query()->findOrFail($therapistId);

        return $therapist->appointments()->with('user:name')->paginate($limit);
    }

    public function setAvailableHours(int $therapistId, string $time): array
    {

        $therapist = $this->model->query()->findOrFail($therapistId);

        $time = Carbon::createFromFormat('H:i', $time, $therapist->timezone)
            ->timezone('UTC')->format('H:i');

        $availableHours = (array)$therapist->available_hours;
        if (!in_array($time, $availableHours)) {
            $availableHours[] = $time;
            $therapist->available_hours = $availableHours;
            $therapist->save();

        }

        return $this->getAvailableHours($therapistId);

    }

    public function getAvailableHours(int $therapistId): array
    {
        $therapist = $this->model->query()->findOrFail($therapistId);
        return collect($therapist->available_hours ?? [])
            ->map(function ($time) use ($therapist) {
                return Carbon::createFromFormat('H:i', $time, 'UTC')->timezone($therapist->timezone)->format('H:i');
            })->toArray();
    }
}
