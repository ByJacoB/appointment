<?php

namespace App\Repositories\Eloquent;

use App\Models\User;
use App\Repositories\Eloquent\Interfaces\UserRepositoryInterface;
use Carbon\Carbon;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    public function appointments(int $userId, int $limit = 24): mixed
    {
        $user = $this->model->query()->findOrFail($userId);

        return $user->appointments()->with('therapist')->paginate($limit);
    }

    public function createAppointment(int $userId, array $data): mixed
    {
        $user = $this->model->query()->findOrFail($userId);

        $data['time'] = $this->convertTimezone($data['time'], $user->timezone);

        return $user->appointments()->create($data);
    }

    public function checkIfAppointmentExists(int $userId, array $data): bool
    {
        $user = $this->model->query()->findOrFail($userId);

        $time = $this->convertTimezone($data['time'], $user->timezone);

        $appointment = $user->appointments()->where('therapist_id', $data['therapist_id'])
            ->where('date', $data['date'])
            ->where('time', $time)
            ->first();

        return (bool)$appointment;
    }


    public function getAvailableHours($availableHours, string $userTimezone): array
    {
        return collect($availableHours ?? [])
            ->map(function ($time) use ($userTimezone) {
                return Carbon::createFromFormat('H:i', $time, 'UTC')->timezone($userTimezone)->format('H:i');
            })->toArray();
    }

    private static function convertTimezone($time, $timezone): string
    {
        return Carbon::createFromFormat('H:i', $time, $timezone)
            ->timezone('UTC')->format('H:i');
    }
}
