<?php

namespace App\Providers;

use App\Repositories\Eloquent\BaseRepository;
use App\Repositories\Eloquent\Interfaces\AppointmentRepositoryInterface;
use App\Repositories\Eloquent\Interfaces\EloquentRepositoryInterface;
use App\Repositories\Eloquent\Interfaces\TherapistRepositoryInterface;
use App\Repositories\Eloquent\Interfaces\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(UserRepositoryInterface::class);
        $this->app->bind(TherapistRepositoryInterface::class);
        $this->app->bind(AppointmentRepositoryInterface::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
