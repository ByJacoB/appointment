<?php

namespace App\Http\Controllers\Therapist;

use App\Helpers\Output;
use App\Http\Controllers\Controller;
use App\Http\Requests\Therapist\SetAvailableHourRequest;
use App\Http\Resources\Therapist\AppointmentResource;
use App\Repositories\Eloquent\TherapistRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Config;

class AppointmentController extends Controller
{
    public function __construct(private readonly TherapistRepository $therapistRepository)
    {
    }

    /**
     * Appointment Index.
     *
     * Allows therapist to view their appointments.
     * @group Therapist Appointments
     * @authenticated
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $therapistId = auth('therapist')->id();
        $appointments = $this->therapistRepository->appointments($therapistId);
        $availableHours = $this->therapistRepository->getAvailableHours($therapistId);

        $data['appointments'] = AppointmentResource::collection($appointments);
        $data['availableHours'] = $availableHours;

        return Output::success(data: $data, message: 'Therapist appointments');
    }

    /**
     * Appointment Set Available Hours.
     *
     * Allows therapist to view their appointment.
     * @group Therapist Appointments
     * @authenticated
     * @param SetAvailableHourRequest $request
     * @return JsonResponse
     */
    public function setAvailableHours(SetAvailableHourRequest $request): JsonResponse
    {
        $requestValidated = $request->validated();
        $authTherapistId = auth('therapist')->id();

        $availableHours = $this->therapistRepository->setAvailableHours($authTherapistId, $requestValidated['time']);

        $data['availableHours'] = $availableHours;

        return Output::success(data: $data, message: 'Therapist available hours set');
    }
}
