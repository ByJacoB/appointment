<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Output;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Repositories\Eloquent\TherapistRepository;
use App\Repositories\Eloquent\UserRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Client;

class AuthController extends Controller
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly TherapistRepository $therapistRepository,
        private readonly Client $client,
    ) {
    }

    /**
     * Register.
     *
     * Allows registration of a new user.
     * @group Authentication
     * @bodyParam name string required The name of the user. Example: John Doe
     * @bodyParam email string required The email of the user. Example:
     * @bodyParam password string required The password of the user. Example: password
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        $this->userRepository->create($request->validated());

        return Output::success(message: 'User created successfully');
    }

    /**
     * Login.
     *
     * Allows login of a user.
     * @group Authentication
     * @bodyParam email string required The email of the user. Example: therapist@laravel.com
     * @bodyParam password string required The password of the user. Example: password
     * @bodyParam provider string required The provider of the user. Example: therapists
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        try {
            $email = $request->input('email');
            $password = $request->input('password');
            $provider = $request->input('provider');

            $client = $this->client->query()
                ->where('password_client', 1)
                ->where('provider', $provider)
                ->first();

            $proxy = Request::create(
                'oauth/token',
                'POST',
                [
                    'username' => $email,
                    'password' => $password,
                    'grant_type' => 'password',
                    'client_id' => $client->id,
                    'client_secret' => $client->secret,
                    'scope' => '*'
                ]
            );

            app()->instance('request', $proxy);

            $response = Route::dispatch($proxy);

            if ($response->getStatusCode() != 200) {
                throw new \Exception(__('Giriş işlemi başarısız.'));
            }

            $data = json_decode($response->getContent(), true);

            match ($provider) {
                'therapists' => $data['user'] = new UserResource(self::getTherapist($email)),
                'users' => $data['user'] = new UserResource(self::getUser($email))
            };

            return Output::success(data: $data, message: __('Giriş işlemi başarıyla tamamlandı.'));
        } catch (\Exception $exception) {
            return Output::error(message: $exception->getMessage());
        }
    }

    private function getTherapist($email): Model|Builder|null
    {
        return $this->therapistRepository->query()->where('email', $email)->first();
    }

    private function getUser($email): Model|Builder|null
    {
        return $this->userRepository->query()->where('email', $email)->first();
    }
}
