<?php

namespace App\Http\Controllers\User;

use App\Helpers\Output;
use App\Http\Controllers\Controller;
use App\Http\Resources\User\TherapistResource;
use App\Repositories\Eloquent\TherapistRepository;
use Illuminate\Http\JsonResponse;

class TherapistController extends Controller
{
    public function __construct(private readonly TherapistRepository $therapistRepository)
    {
    }

    /**
     * Therapist Index.
     *
     * Allows user to view all therapists.
     * @group User Therapists
     * @authenticated
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $therapists = $this->therapistRepository->paginate();

        $data['therapists'] = TherapistResource::collection($therapists);

        return Output::success(data: $data, message: 'Therapists');
    }

    /**
     * Therapist Show.
     *
     * Allows user to view a therapist.
     * @group User Therapists
     * @authenticated
     * @urlParam id required The ID of the id Example:1.
     * @return JsonResponse
     */
    public function show(int $therapist): JsonResponse
    {
        $therapist = $this->therapistRepository->find($therapist);

        $data['therapist'] = TherapistResource::make($therapist);

        return Output::success(data: $data, message: 'Therapist');
    }
}
