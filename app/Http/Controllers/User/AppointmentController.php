<?php

namespace App\Http\Controllers\User;

use App\Helpers\Output;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\AppointmentStoreRequest;
use App\Http\Resources\User\AppointmentResource;
use App\Repositories\Eloquent\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    public function __construct(private readonly UserRepository $userRepository)
    {
    }

    /**
     * Appointment Index.
     *
     * Allows user to view their appointments.
     * @group User Appointments
     * @authenticated
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $userId = auth('api')->id();

        $appointments = $this->userRepository->appointments($userId);

        $data['appointments'] = AppointmentResource::collection($appointments);

        return Output::success(data: $data, message: 'User appointments');
    }

    /**
     * Appointment Store.
     *
     * Allows user to create an appointment.
     * @group User Appointments
     * @authenticated
     * @bodyParam therapist_id int required The ID of the therapist Example:1
     * @bodyParam date string required The date of the appointment Example:2021-10-10
     * @bodyParam time string required The time of the appointment Example:10:00
     * @return JsonResponse
     */
    public function store(AppointmentStoreRequest $request): JsonResponse
    {
        $userId = auth('api')->id();

        $checkIfAppointmentExists = $this->userRepository->checkIfAppointmentExists($userId, $request->validated());

        if ($checkIfAppointmentExists) {
            return Output::error(message: 'Appointment already exists');
        }

        $appointment = $this->userRepository->createAppointment($userId, $request->validated());

        $data['appointment'] = AppointmentResource::make($appointment);

        return Output::success(data: $data, message: 'Appointment created');
    }
}
