<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Response;

class TherapistMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response) $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (auth('therapist')->check()) {
            Config::set('app.timezone', auth('therapist')->user()->timezone); // Diğer bir örnek timezone
        }

        return $next($request);
    }
}
