<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    public function __construct(
        private readonly Artisan $artisan,
        private readonly DB      $db,
        private readonly Schema  $schema
    )
    {
    }

    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->schema::disableForeignKeyConstraints();
        $this->call([
            UserSeeder::class,
            TherapistSeeder::class
        ]);
        $this->schema::enableForeignKeyConstraints();

        $this->artisan::call('passport:install --force');
        $this->db::table('oauth_clients')->where('provider', 'users')->update(['provider' => 'users']);

        $this->artisan::call('passport:install --force');
        $this->db::table('oauth_clients')->where('provider', 'users')
            ->orderBy('id', 'desc')->limit(1)->update(['provider' => 'therapists']);

    }
}
