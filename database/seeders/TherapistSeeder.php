<?php

namespace Database\Seeders;

use App\Repositories\Eloquent\TherapistRepository;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class TherapistSeeder extends Seeder
{
    public function __construct(private readonly TherapistRepository $therapistRepository)
    {
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->therapistRepository->truncate();

        $this->therapistRepository->create([
            'name' => 'Therapist 1',
            'email' => 'therapist@laravel.com',
            'password' => Hash::make('password'),
            'timezone' => 'America/Los_Angeles',
            'available_hours' => array()
        ]);
    }
}
