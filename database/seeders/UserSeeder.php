<?php

namespace Database\Seeders;

use App\Repositories\Eloquent\UserRepository;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{

    public function __construct(private readonly UserRepository $userRepository)
    {
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->userRepository->truncate();

        $this->userRepository->create([
            'name' => 'User 1',
            'email' => 'user@laravel.com',
            'password' => Hash::make('password'),
            'timezone' => 'America/New_York'
        ]);
    }
}
