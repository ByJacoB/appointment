<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\User\AppointmentController as UserAppointmentController;
use App\Http\Controllers\Therapist\AppointmentController as TherapistAppointmentController;
use App\Http\Controllers\User\TherapistController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Auth routes
Route::prefix('auth')->controller(AuthController::class)->group(static function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
});

// User routes
Route::prefix('user')->middleware('auth:api')->group(static function () {
    Route::apiResource('appointments', UserAppointmentController::class)->only('index', 'store');
    Route::apiResource('therapists', TherapistController::class)->only('index', 'show');
});

// Therapist routes
Route::prefix('therapist')->middleware(['auth:therapist', 'therapist'])->group(static function () {
    Route::apiResource('appointments', TherapistAppointmentController::class)->only('index');
    Route::post('appointments/set-available-hours', [TherapistAppointmentController::class, 'setAvailableHours']);
});
